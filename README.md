GSHEET RESWUE Importer
======================
This script generates a json encoded string which contains all agents of an operation. It can easily be imported into reswue.

HOWTO
-----
1. Open the spreadsheet with the agent names in it. Make sure that they are arranged in a rectangle, column or row.
2. Goto "Tools" -> "Script Editor"
3. Empty the code area and insert the contents of "gsheet-reswue-importer.js"
4. Save and press the "play" button (you'll maybe asked for authorisation of the script to access the data)
5. Switch back to the Spreadsheet and enter the Range of your agent names in A1-Notation", e.g. "A1:A7".
6. After a few seconds, the JSON string will show up. Copy it into the "import" textbox of RESWUE (found at "Tools" => "Import/Export").

Have fun with your OP!
@Clarity97
https://plus.google.com/+JanFelixWiebe

#RESWUEisAwesome