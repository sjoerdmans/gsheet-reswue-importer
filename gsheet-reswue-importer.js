function gSheetImporter() {
  var sheet = SpreadsheetApp.getActiveSpreadsheet();
  var range = Browser.inputBox('Please select the area of the agent names (e.g. "B1:B5"):');
  
  if (range != "" && range != "cancel") {
    var cells = sheet.getRange(range).getValues(); // get all Agent names 
    
    var agents = new Array();
    for (row = 0, len = cells.length; row < len; row++) {
      if (cells[row].toString() != "") {
        agents[row] = {"name": cells[row].toString().trim(), "role": "10"}; // add every agent to array (role 10 means "field agent")
      }
    }
  
    var json = {"Document": {"FileFormatVersion": "1"}, "Agents": agents};
    var json_string = JSON.stringify(json);
    Browser.msgBox(json_string);
  } else {
    sheet.toast("Please enter a range!");
  }
}
